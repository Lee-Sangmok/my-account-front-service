/*jshint esversion: 6 */

//--- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cookieParser = require('cookie-parser');
const util = require(__BASEDIR+'/util');
//-----------

//---- 기본 library 셋팅
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(cookieParser());
//--------------

router.get("/", (req, res) => {
	res.redirect("/master-accounts");
});



//--- 종합계좌 정보 가져오기 
router.get("/master-accounts", (req, res) => {
	util.log("Master account info get page!");	
	res.render("bizlogic/master-accounts", {
		mode: "init",
		masterlist: [],
		message: "",
		type : "",
		no: ""
	});   
});

router.post("/master-accounts", (req, res) => {
    util.log("종합계좌 정보 가져오기");
  	let body = req.body;
	util.log("req=>"+__BIZ_API_URI+"/master-accounts");
	util.log("type:"+body.type+",no:"+body.no);

	axios.get(__BIZ_API_URI+"/master-accounts",
	{
        params: {
            type : body.type,
			no: body.no
        }	
	})
	.then((ret) => {
		util.log("SUCCESS!");
		util.log(ret);
		util.log(ret.data.masterlist);
		util.log("계좌번호 "+ ret.data.masterlist.accountNumber);
		
        res.render("bizlogic/master-accounts", {
			mode: "view",
            masterlist: ret.data.masterlist,
			message: ret.data.message,
			type : body.type,
			no: body.no
       });
    })
	.catch((error) => {
		util.log("ERROR!");
		console.log(error);
		res.redirect("/master-accounts");

	});	    
});
//--------------

router.get("/master-accounts-post", (req, res) => {
	util.log("Go Input 종합계좌개설!");	
	
	res.render("bizlogic/master-accounts-post",
	{
		rmnNo: 			"",
		accountName: 	"",
		password: 		""        
	});

});

//--- 종합계좌 개설 정보입력 로직
router.post("/master-accounts-post", (req, res) => {
	util.log("종합계좌 개설 Process");
	let body = req.body;
	
	axios.post(__BIZ_API_URI+"/master-accounts", 
		{
                rmnNo: 			body.rmnNo,
                accountName: 	body.accountName,
                password: 		body.password        
 		}
	)
	.then((ret) => {
		util.log(ret);
		util.log(ret.data);
		util.log(ret.data.masterAccountPost);
		res.render("bizlogic/master-accounts-post",
		{
 			rmnNo: 			ret.data.masterAccountPost.rmnNo,
			accountName: 	ret.data.masterAccountPost.accountName,
			password: 		ret.data.masterAccountPost.password,
			accountNumber:  ret.data.masterAccountPost.accountNumber,        
			message: 		ret.data.message		
		});
	})
	.catch((error) => {
		console.error(error);
		res.redirect("/master-accounts-post");
	});	
});
//----------------

module.exports = router;
