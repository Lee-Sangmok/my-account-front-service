/*jshint esversion: 6 */

//--- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cookieParser = require('cookie-parser');
const util = require(__BASEDIR+'/util');
//-----------

//---- 기본 library 셋팅
const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(cookieParser());
//--------------

//--- 개별 계좌 정보 가져오기 
router.get("/sub-accounts", (req, res) => {
	util.log("Go sub account get page!");	
	res.render("bizlogic/sub-accounts", {
		mode: "init",
		sublist: [],
		message: "",
		type : "",
		no: ""
	});   
});

router.post("/sub-accounts", (req, res) => {
	util.log("개별계좌 정보 가져오기");
	let body = req.body;
	util.log("req=>"+__BIZ_API_URI+"/sub-accounts");
	util.log("type:"+body.type+",no:"+body.no);
	
	axios.get(__BIZ_API_URI+"/sub-accounts",
	{
        params: {
            type : body.type,
			no: body.no
        }	
 	})
 	.then((ret) => {
		util.log("개별 계좌 조회 !");
		util.log(ret);
		util.log(ret.data.sublist);
		util.log("masterAccountId="+ ret.data.sublist.masterAccountId);
		
        res.render("bizlogic/sub-accounts", {
			mode: "view",
            sublist: ret.data.sublist,
			message: ret.data.message,
			type : body.type,
			no: body.no
		});
	})
	.catch((error) => {
		util.log("ERROR!");
		console.log(error);
		res.redirect("/sub-accounts");

	});	    
});	   
		
router.get("/sub-accounts-post", (req, res) => {
	util.log("Go Input 개별계좌개설!");	

	res.render("bizlogic/sub-accounts-post",
	{
		masterAccountNumber: 	"",
		type: 					"",        
		accountName: 			"",
		tradingTax: 			"",
		isPreMargin: 			""
	});
});

//--- 개별 계좌 정보입력 로직
router.post("/sub-accounts-post", (req, res) => {
	util.log("개별계좌 개설 Process");
	let body = req.body;
	
	axios.post(__BIZ_API_URI+"/sub-accounts", 
		{
			masterAccountNumber: 	body.masterAccountNumber,
			type: 					body.type,        
			accountName: 			body.accountName,
			tradingTax: 			body.tradingTax,
			isPreMargin: 			body.isPreMargin
		}
	)
	.then((ret) => {
		util.log(ret);
		util.log(ret.data);
		util.log(ret.data.subAccountPost);
		res.render("bizlogic/sub-accounts-post",
		{
			masterAccountNumber: 	ret.data.subAccountPost.masterAccountNumber,
			type: 					ret.data.subAccountPost.type,        
			accountName: 			ret.data.subAccountPost.accountName,
			tradingTax: 			ret.data.subAccountPost.tradingTax,
			isPreMargin: 			ret.data.subAccountPost.isPreMargin,
			accountNumber:  		ret.data.subAccountPost.accountNumber,        
			message: 				ret.data.message		

		});
	})
	.catch((error) => {
		console.error(error);	
		res.redirect("/sub-accounts-post");
	});	
});
//----------------

module.exports = router;
