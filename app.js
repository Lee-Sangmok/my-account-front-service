/*jshint esversion: 6 */
//--- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const axios = require('axios');
const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser');
const util = require(__dirname+'/util');
//----------

//--- global constants & 환경변수
global.__BASEDIR = __dirname + '/';
global.__ACCESS_TOKEN_NAME = "x-access-token";
global.__AUTH_API_URI = process.env.AUTH_API_URI || "http://login-api.169.56.164.52.nip.io";
//global.__BIZ_API_URI = process.env.BIZ_API_URI || "http://kucb.169.56.164.52.nip.io";
global.__BIZ_API_URI = process.env.BIZ_API_URI || "http://account-service-app.169.56.164.52.nip.io";
global.__BIZ_ACC_API_URI = process.env.BIZ_ACC_API_URI || "http://account-service-app.169.56.164.52.nip.io";
global.__DASHBOARD_API_URI = process.env.DASHBOARD_API_URI || "http://dashboard.169.56.164.52.nip.io/api/dashboard";
const JWT_SECRET = process.env.JWT_SECRET || "MySecretKey";
const port = (process.env.PORT || 8093);
//--------

//---- 기본 library 셋팅
const app = express();
app.use(express.static(path.join(__BASEDIR, '/public')));		//static resource 폴더 
app.use(bodyParser.urlencoded({extended:false}));				//include request 객체 parser
app.use(cookieParser());										//include cookie parser
//-----------

//--- ejs(Embed JS) 환경 셋팅
app.set('view engine','ejs');							//ui page rendering 시 ejs 사용
app.set('views', path.join(__BASEDIR, '/templates'));	//ui rendering시 사용할 ejs파일 위치 지정
//-------------

//----- middle ware: routing되는 서버모듈 시작 전에 항상 수행-인증토큰 검증
app.use(function(req, res, next) {
	let pathname = req.url;
    util.log("Request for [" + pathname + "] received.");
	
	//-- root path는 liveness, readiness probe임
	if(pathname === "/alive") {
		res.writeHead(200, { 'Content-Type':'text/html; charset=utf-8' });
		res.write('I am alive');
		res.end();
		next();
		return;
	}
	
    next();

});
//-------------

//--- include 개발 모듈
app.use(require(path.join(__BASEDIR, "/routes/master-accounts.js")));		//include 
app.use(require(path.join(__BASEDIR, "/routes/sub-accounts.js")));		//include 
//--------

//----- start web server 
app.listen(port, () => {
	console.log('Listen: ' + port);
});
//----------------