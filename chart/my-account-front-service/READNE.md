
# mvp-my-account-front-service

[mvp-my-account-front-service](https://github.com/happycloudpak/helm-charts/tree/master/stable)
MVP시 참조하기 위한 Java기반 API서비스입니다.

## Introduction

MVP시 참조하기 위한 Java기반 API서비스입니다.


## Prerequisites

- Kubernetes 1.8+

## Installing the Chart

To install the chart with the release name `my-release`:

```bash
$ $ helm install <git repository>/mvp-my-account-front-service --name release-mvp-my-account-front-service

```

The command deploys this app on the Kubernetes cluster in the default configuration. The [configuration](#configuration) section lists the parameters that can be configured during installation.

> **Tip**: List all releases using `helm list`

## Uninstalling the Chart

To uninstall/delete the `release-mvp-my-account-front-service` deployment:

```bash
$ helm delete release-mvp-my-account-front-service --purge
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

## Configuration
See config.yaml 
Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`. For example,

